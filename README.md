# JShadow 

JShadow is a bytecode instrumentation tool. It was developed on BCEl5.2 to instrument code to track its execution as well as allow a user to update library code to simplify modeling of the library for testing and  verification purposes.

## Dependencies
* BCel 5.2
* Xstream 1.4.8

## Set up ##

Import this as an Eclipse project. 
Run it from a Linux terminal, further usage instructions will appear.

## Examples

**InstrumentInOutTransformer.java**
Injects log statements into method that logs the method's input parameters and return value. It transforms this class:
```java
public class TestIOCls
{    
    public void onReceive(final String s, final Reader R) {

        if (s.equals("test")) {
            System.out.println("Test1");
        }
        else {
            System.out.println("Test2");
        }
    }

}
```
into:
```java
public class TestIOCls
{
    public TestIOCls() {
        final Object[] array = new Object[0];
        final String paramString = FlowLogger.getParamString("()V", array);

        FlowLogger.logMethod("com.test.TestIOCls", "<init>", "V", "()V", null, paramString);
    }
    
    public void onReceive(final String s, final Reader R) {
        final Object[] array = { s, R };
        final String paramString = FlowLogger.getParamString("(Ljava/lang/String;Ljava/io/Reader;)V", array);

        if (s.equals("test")) {
            System.out.println("Test1");
        }
        else {
            System.out.println("Test2");
        }

        FlowLogger.logMethod("com.test.TestIOCls", "onReceive", "V", "(Ljava/lang/String;Ljava/io/Reader;)V", null, paramString);
    }

}
```


## Who do I talk to? ##

S.E.A. (Sean) Nel or 
Heila van der Merwe (E-mail: heilamvdm@gmail.com)

##Extra: ##

**Running**
```sh
java -cp bin JShadow --verbose --inout --transform com.example.android.musicplayer.MusicIntentReceiver --in "<path-to>/RandomMusicPlayer/bin/classes" --out "gen/RandomMusicPlayer"
```

**Decompile results**
Using procyon:
```sh
java -jar ../dec.jar com/test/TestIOCls.class
```
or JAD decompiler:
```sh
java -jar JAD.jar com/test/TestIOCls.class
```
or javap: 
```sh
javap -v com/test/TestIOCls.class
```

