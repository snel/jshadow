package jshadow;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;

/**
 * Replaces class methods with modelled versions. The models are built from serialized output from an android
 * application instrumented with FlowLogger logs. This instrumentation can be done with JShadow using the
 * ShadowMethod- and NativeInvokeTransformers.
 * 
 * @author S. E. A. Nel
 */
public class ModelTransformer implements Transformer<ClassGen> {
  /**
   * Applies the transformation to the specified class.
   * 
   * @param c
   *          Class to apply the transformation to.
   */
  @Override
  public void transform(ClassGen c) {
    try {
      ConstantPoolGen cp = c.getConstantPool();
      // InstructionFactory factory = new InstructionFactory(c);

      for (Method method : c.getMethods()) {
        MethodGen m = new MethodGen(method, c.getClassName(), cp);
        InstructionList body = m.getInstructionList();

        // Skips non-concrete methods.
        if (body == null)
          continue;

        // TODO

        m.update();
        c.replaceMethod(method, m.getMethod());
      }

      c.update();
    } catch (Throwable e) {
      e.printStackTrace();
      System.exit(0);
    }
  }
}
