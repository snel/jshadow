package jshadow;

import java.util.logging.Level;

import org.apache.bcel.Constants;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.INVOKESTATIC;
import org.apache.bcel.generic.INVOKEVIRTUAL;
import org.apache.bcel.generic.Instruction;
import org.apache.bcel.generic.InstructionFactory;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.InvokeInstruction;
import org.apache.bcel.generic.MethodGen;

import jshadow.utils.Utils;

/**
 * Replaces all method invocations to native methods to shadow methods instead (except those in the shadow
 * methods themselves).
 * 
 * @author S. E. A. Nel
 */
public class ShadowRedirectTransformer implements Transformer<ClassGen> {
  protected ShadowMethodTransformer smt;

  /**
   * Constructor.
   * 
   * @param smt
   *          The ShadowTransformer previously applied.
   */
  public ShadowRedirectTransformer(ShadowMethodTransformer smt) {
    this.smt = smt;
  }

  /**
   * Applies the transformation to the specified class.
   * 
   * @param c
   *          Class to apply the transformation to.
   */
  @Override
  public void transform(ClassGen c) {
    try {
      ConstantPoolGen cp = c.getConstantPool();
      InstructionFactory factory = new InstructionFactory(c);

      for (Method method : c.getMethods()) {
        MethodGen m = new MethodGen(method, c.getClassName(), cp);
        InstructionList body = m.getInstructionList();

        // Skips non-concrete methods.
        if (body == null)
          continue;
        // Skips shadow methods.
        if (smt.isShadow(Utils.getSignature(m)))
          continue;

        JShadow.getInstance().log(Level.FINE, String.format("** %s **", Utils.getSignature(m)));

        for (InstructionHandle i : body.getInstructionHandles()) {
          if (i.getInstruction() instanceof InvokeInstruction) {
            InvokeInstruction instr = (InvokeInstruction) i.getInstruction();

            String signature = Utils.getSignature(instr.getReferenceType(cp).toString(),
                instr.getMethodName(cp), instr.getSignature(cp));

            if (smt.isShadowed(signature)) {
              MethodGen m2 = smt.getMethod(signature);
              transformInvokeInstruction(i, m2, factory);
            }
          }
        }

        m.update();
        c.replaceMethod(method, m.getMethod());
      }

      c.update();
    } catch (Throwable e) {
      e.printStackTrace();
      System.exit(0);
    }
  }

  /**
   * Redirects the invocation to the referenced method's shadow.
   * 
   * @param i
   *          Handle of the instruction to replace.
   * @param m
   *          The referenced method.
   * @param factory
   *          A InstructionFactory instance.
   */
  protected void transformInvokeInstruction(InstructionHandle i, MethodGen m, InstructionFactory factory) {
    JShadow.getInstance().log(Level.FINER,
        String.format("Redirecting %s to %s.", m.getName(), ShadowMethodTransformer.renamed(m)));

    short kind;
    Instruction instruction = i.getInstruction();
    if (instruction instanceof INVOKESTATIC) {
      kind = Constants.INVOKESTATIC;
    } else if (instruction instanceof INVOKEVIRTUAL) {
      kind = Constants.INVOKEVIRTUAL;
    } else {
      throw new RuntimeException(
          String.format("Invoke instruction not supported for %s", instruction.getClass().getSimpleName()));
    }

    Instruction instr = factory.createInvoke(factory.getClassGen().getClassName(),
        ShadowMethodTransformer.renamed(m), m.getReturnType(), m.getArgumentTypes(), kind);
    i.setInstruction(instr);
  }
}
