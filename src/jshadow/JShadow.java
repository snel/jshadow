package jshadow;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;
import java.util.logging.Level;

import org.apache.bcel.classfile.ClassParser;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.MethodGen;

import jshadow.utils.Argument;

/**
 * Loads class files from a JAR archive and applies transformations to it, before saving to a new JAR file.
 * 
 * @author S.E.A. Nel
 */
public class JShadow implements Transformer<ClassGen> {
  public static JShadow instance = new JShadow();
  protected int loggingLevel = Level.INFO.intValue();
  protected ArrayList<Argument> arguments;
  protected List<Transformer<ClassGen>> transformers;
  protected Map<String, ClassGen> classGens;
  /** Classfiles marked for editing. */
  protected HashSet<String> marked;
  protected HashSet<File> inputFiles;
  protected File outputFile;

  public static void main(String[] args) {
    instance.parseArgs(args);
    instance.execute();
  }

  /**
   * Gets the singleton instance of this class.
   * 
   * @return The singleton instance of this class.
   */
  public static JShadow getInstance() {
    return instance;
  }

  /**
   * Constructor.
   */
  public JShadow() {
    transformers = new ArrayList<Transformer<ClassGen>>();
    classGens = new HashMap<String, ClassGen>();
    marked = new HashSet<String>();
    inputFiles = new HashSet<File>();
    loadArguments();
  }

  /**
   * Loads the terminal argument settings.
   */
  protected void loadArguments() {
    arguments = new ArrayList<Argument>();

    arguments.add(new Argument() {
      @Override
      public Argument set(String val) {
        printUsage();
        return this;
      }
    }.setSwitches(new String[] { "-h", "--help" }).setUsageString("Displays this."));

    arguments.add(new Argument() {
      @Override
      public Argument set(String val) {
        setLoggingLevel(Level.ALL.intValue());
        return this;
      }
    }.setSwitches(new String[] { "-v", "--verbose" }).setUsageString("Enables verbose logging."));

    // TODO: parse for method name, to mark only a specified method
    arguments.add(new Argument() {
      @Override
      public Argument set(String val) {
        marked.add(val);
        return this;
      }
    }.setSwitches(new String[] { "-t", "--transform" })
        .setUsageString("Marks the specified class for editing.").setRequired(true));

    arguments.add(new Argument() {
      @Override
      public Argument set(String val) {
        inputFiles.add(new File(val));
        return this;
      }
    }.setSwitches(new String[] { "-i", "--in" }).setUsageString("Adds the specified class."));

    arguments.add(new Argument() {
      @Override
      public Argument set(String val) {
        outputFile = new File(val);
        return this;
      }
    }.setSwitches(new String[] { "-o", "--out" }).setUsageString("Output path.").setRequired(true));

    arguments.add(new Argument() {
      @Override
      public Argument set(String val) {
        ShadowMethodTransformer smt = new ShadowMethodTransformer() {
          protected boolean shouldShadow(MethodGen m) {
            return m.isNative();
          }
        };
        transformers.add(smt);
        transformers.add(new ShadowRedirectTransformer(smt));
        return this;
      }
    }.setSwitches(new String[] { "--native" }).setUsageString(
        "Instruments native methods to print parameter and return values."));

    arguments.add(new Argument() {
      @Override
      public Argument set(String val) {
        InstrumentInOutTransformer smt = new InstrumentInOutTransformer();
        transformers.add(smt);
        return this;
      }
    }.setSwitches(new String[] { "--inout" }).setUsageString(
        "Instruments all methods to print parameter and return values."));

    arguments.add(new Argument() {
      @Override
      public Argument set(String val) {
        transformers.add(new PublifierTransformer());
        return this;
      }
    }.setSwitches(new String[] { "--publify" }).setUsageString(
        "Sets all methods to be public (instead of private, protected or final)."));

    arguments.add(new Argument() {
      @Override
      public Argument set(String val) {
        transformers.add(new StubMethodTransformer());
        return this;
      }
    }.setSwitches(new String[] { "--stub" }).setUsageString("Stubs all native methods."));

    Collections.sort(arguments);
  }

  /**
   * Prints help for using the program from a terminal to stdout.
   */
  protected void printUsage() {
    System.out.println("Usage:");
    for (int i = 0; i < arguments.size(); i++) {
      System.out.println(arguments.get(i).getUsageString());
    }
  }

  /**
   * Parses arguments given from the terminal.
   * 
   * @param args
   *          The terminal arguments.
   */
  protected void parseArgs(String[] args) {
    try {
      Argument[] arg_array = new Argument[args.length];
      // Matches the arguments (leaves entries null if unmatched).
      for (int i = 0; i < args.length; i++) {
        for (int j = 0; j < arguments.size(); j++) {
          Argument argument = arguments.get(j);
          String[] switches = argument.getSwitches();
          for (int k = 0; k < switches.length; k++) {
            if (switches[k].equals(args[i])) {
              arg_array[i] = argument;
              j = arguments.size(); // breaks the 'j' for loop.
              break; // breaks the 'k' for loop.
            }
          }
        }
      }

      HashSet<Argument> setArguments = new HashSet<Argument>();
      // Reads the arguments and sets their values.
      for (int i = 0; i < args.length; i++) {
        Argument arg = arg_array[i];
        if (arg == null) {
          throw new RuntimeException(String.format("Unrecognized argument: %s.", args[i]));
        }

        // If the next entry is null, it is not an argument, so it is the value for the current argument.
        if (i < args.length - 1 && arg_array[i + 1] == null) {
          arg.set(args[++i]);
        } else if (arg.isRequired()) {
          throw new RuntimeException(String.format("%s argument is missing a value.", args[i]));
        } else {
          arg.set(null);
        }
        setArguments.add(arg);
      }

      // Verifies that all required arguments were specified.
      for (int i = 0; i < arguments.size(); i++) {
        Argument arg = arguments.get(i);
        if (arg.isRequired() && !setArguments.contains(arg)) {
          throw new RuntimeException(String.format("%s is a required argument.", arg));
        }
      }
    } catch (IllegalArgumentException e) {
      System.err.println(e.getMessage());
      printUsage();
      System.exit(1);
    }
  }

  /**
   * Sets the logging level/verbosity.
   * 
   * @param loggingLevel
   *          The logging level setting. Higher means more verbose.
   */
  public void setLoggingLevel(int loggingLevel) {
    this.loggingLevel = loggingLevel;
  }

  /**
   * Prints the message to stdout.
   * 
   * @param msg
   *          The message to print.
   */
  public void log(String msg) {
    log(Level.INFO, msg);
  }

  /**
   * Prints the message to stdout.
   * 
   * @param msg
   *          The message to print.
   */
  public void log(Level level, String msg) {
    if (level.intValue() < loggingLevel) {
      return;
    }
    System.out.println(String.format("%s  %s", new SimpleDateFormat("HH:mm:ss").format(new Date()), msg));
  }

  /**
   * Returns true iff the file is a JAR file.
   * 
   * @param file
   *          File to check.
   * @return true iff the file is a JAR file.
   */
  protected boolean isJARFile(File file) {
    return file.getName().toLowerCase().endsWith(".jar");
  }

  /**
   * Returns true iff the file is a class file.
   * 
   * @param file
   *          File to check.
   * @return true iff the file is a class file.
   */
  protected boolean isClassFile(File file) {
    return file.getName().toLowerCase().endsWith(".class");
  }

  /**
   * Executes JShadow against the input parameters.
   */
  protected void execute() {
    log(String.format("%s started.", JShadow.class.getName()));

    // Injects input files.
    String s = null;
    try {
      for (File f : inputFiles) {
        s = f.toString();
        add(f);
      }
    } catch (Exception e) {
      System.err.println(String.format("Error injecting %s.", s));
      e.printStackTrace();
      System.exit(1);
    }

    // Applies transformations to files marked for editing.
    try {
      for (String x : marked) {
        s = x;
        ClassGen c = classGens.get(x);
        if (c != null) {
          transform(c);
        }
      }
    } catch (Exception e) {
      System.err.println(String.format("Error transforming %s.", s));
      e.printStackTrace();
      System.exit(1);
    }

    // Writes output to disk.
    try {
      if (isJARFile(outputFile)) {
        saveJar();
      } else {
        saveClasses();
      }
    } catch (Exception e) {
      System.err.println(String.format("Error saving output to %s.", outputFile));
      e.printStackTrace();
      System.exit(1);
    }

    log(String.format("%s finished.", JShadow.class.getName()));
  }

  /**
   * Adds the classes contained in the path or JAR file specified.
   * 
   * @param file
   *          A class file or path containing class files.
   * @throws Exception
   */
  public void add(File file) throws Exception {
    if (file.isDirectory()) {
      File[] list = file.listFiles();
      if (list != null) {
        for (File f : list) {
          add(f);
        }
      }
    } else if (isClassFile(file)) {
      JavaClass c = new ClassParser(new FileInputStream(file), file.getName()).parse();
      addClass(c);
    } else if (isJARFile(file)) {
      JarFile jarFile = new JarFile(file);
      try {
        Enumeration<JarEntry> entries = jarFile.entries();
        while (entries.hasMoreElements()) {
          JarEntry entry = entries.nextElement();
          if (entry != null && entry.getName().endsWith(".class")) {
            JavaClass c = new ClassParser(jarFile.getInputStream(entry), entry.getName()).parse();
            addClass(c);
          }
        }
      } finally {
        try {
          jarFile.close();
        } catch (Exception e) {
          // We don't care.
        }
      }
    }
  }

  /**
   * Adds the specified class into the table of classes for export.
   * 
   * @param jc
   *          The class to export.
   */
  protected void addClass(JavaClass jc) {
    classGens.put(jc.getClassName(), new ClassGen(jc));
  }

  /**
   * Applies all added transformers to the specified class. For internal use.
   * 
   * @param c
   *          Class to transform.
   * @throws Exception
   */
  public void transform(ClassGen c) throws Exception {
    log(Level.FINE, String.format("* %s *", c.getClassName()));

    for (Transformer<ClassGen> t : transformers) {
      log(Level.FINEST, String.format("Applying %s...", t.getClass().getName()));
      t.transform(c);
    }
  }

  /**
   * Saves the transformed class files to a JAR file at the specified location. Assumes the directory
   * specified in the path exists.
   * 
   * @throws Exception
   */
  public void saveClasses() throws Exception {
    for (ClassGen c : classGens.values()) {
      Path path = outputFile.toPath().resolve(c.getClassName().replace('.', '/') + ".class").toAbsolutePath();
      path.getParent().toFile().mkdirs();

      String f = path.toString();
      try {
        c.getJavaClass().dump(f);
      } catch (Exception e) {
        throw new Exception("Error saving class file to %s.", e);
      }
    }
  }

  /**
   * Saves the transformed class files to a JAR file at the specified location. (Assumes the directory
   * specified in the path exists).
   * 
   * @throws Exception
   */
  public void saveJar() throws Exception {
    JarOutputStream jos = null;
    try {
      outputFile.getParentFile().mkdirs();
      jos = new JarOutputStream(new FileOutputStream(outputFile));

      for (ClassGen c : classGens.values()) {
        jos.putNextEntry(new JarEntry(c.getClassName().replace('.', '/') + ".class"));
        jos.write(c.getJavaClass().getBytes());
        jos.closeEntry();
        jos.flush();
      }

      jos.closeEntry(); // Why is this here again?
    } finally {
      try {
        jos.close();
      } catch (Exception e) {
        // We don't care.
      }
    }
  }
}