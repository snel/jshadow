package jshadow;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;

import jshadow.utils.Utils;

import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.Instruction;
import org.apache.bcel.generic.InstructionFactory;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.NOP;
import org.apache.bcel.generic.ReturnInstruction;
import org.apache.bcel.generic.TargetLostException;
import org.apache.bcel.generic.Type;

/**
 * Adds log statements detailing the values passed to methods as parameters and their return values.
 * 
 * Example: It transforms
 * 
 * <pre>
 * public class TestCls {
 * 
 *   public void onReceive(final String s, final Reader R) {
 *     if (s.equals(&quot;test&quot;)) {
 *       System.out.println(&quot;Test1&quot;);
 *     } else {
 *       System.out.println(&quot;Test2&quot;);
 *     }
 *   }
 *   
 * }
 * </pre>
 * 
 * Into:
 * 
 * <pre>
 * public class TestCls {
 *   public TestCls() {
 *     final Object[] array = new Object[0];
 *     final String paramString = FlowLogger.getParamString(&quot;()V&quot;, array);
 * 
 *     FlowLogger.logMethod(&quot;com.test.TestCls&quot;, &quot;&lt;init&gt;&quot;, &quot;V&quot;, &quot;()V&quot;, null, paramString);
 *   }
 * 
 *   public void onReceive(final String s, final Reader R) {
 * 
 *     final Object[] array = { s, R };
 *     final String paramString = FlowLogger.getParamString(&quot;(Ljava/lang/String;Ljava/io/Reader;)V&quot;, array);
 * 
 *     if (s.equals(&quot;test&quot;)) {
 *       System.out.println(&quot;Test1&quot;);
 *     } else {
 *       System.out.println(&quot;Test2&quot;);
 *     }
 * 
 *     FlowLogger.logMethod(&quot;com.test.TestCls&quot;, &quot;onReceive&quot;, &quot;V&quot;, &quot;(Ljava/lang/String;Ljava/io/Reader;)V&quot;, null,
 *         paramString);
 *   }
 *   
 * }
 * </pre>
 * 
 * @author S. E. A. Nel, Heila
 */
public class InstrumentInOutTransformer implements Transformer<ClassGen> {

  public InstrumentInOutTransformer() {
  }

  /**
   * Applies the transformation to all methods except for constructor
   * 
   * @param c
   *          Class to apply the transformation to.
   */
  @Override
  public void transform(ClassGen c) {
    try {
      for (Method method : c.getMethods()) {
        if (!method.getName().contains("<init>")) { // no not instrument constructors
          MethodGen m = new MethodGen(method, c.getClassName(), c.getConstantPool());
          try {

            instrumentMethod(m, c);
            c.replaceMethod(method, m.getMethod());
            c.update(); // ?
          } catch (Exception e) {
            JShadow.getInstance().log(Level.SEVERE,
                String.format("Error enstrumenting %s.", Utils.getSignature(m)));
            throw (e);
          }
          m.update();
        }
        c.update();
      }
    } catch (Throwable e) {
      e.printStackTrace();
      System.exit(0);
    }
  }

  /**
   * Instruments the given method with param and return value logs.
   * 
   * @param method
   *          The method to instrument.
   * @param c
   *          The class that the method belongs to.
   * @throws TargetLostException
   */
  @SuppressWarnings("unchecked")
  protected void instrumentMethod(MethodGen method, ClassGen c) {
    JShadow.getInstance().log(Level.FINE, String.format("Instrumenting %s.", Utils.getSignature(method)));

    StatementFactory sfactory = new StatementFactory(c);
    InstructionList body = method.getInstructionList();

    // Stores the parameter values as a String. This has to be at the start of
    // the method incase they get changed. Calls to Flowlogger to serialize
    InstructionList il = new InstructionList(); // list to insert into current instructions
    int parameterValues = sfactory.appendStoreParams(il, method);
    body.insert(il); // inserts at start of method

    // Need to inject Flowlogger.log invokestatic byte-code at instructionhandle of each
    // Return byte-code
    List<InstructionHandle> returns = new LinkedList<>();

    // Collect the instruction handlers of the return instruction
    for (Iterator<InstructionHandle> it = body.iterator(); it.hasNext();) {
      InstructionHandle ih = it.next();
      if (ih != null) {
        Instruction i = ih.getInstruction();
        if (i instanceof ReturnInstruction) {
          returns.add(ih);
        }
      }
    }
    // Inject instructions at position of return
    for (InstructionHandle rih : returns) {
      il = new InstructionList();
      // store return value on top of stack
      int returnValue = sfactory.appendStoreReturn(il, method);

      // append log instructions
      sfactory.appendLogMethod(il, parameterValues, returnValue, method);

      // Places the return value back on the stack.
      if (method.getReturnType() != Type.VOID) {
        il.append(InstructionFactory.createLoad(method.getReturnType(), returnValue));
      }

      // Inject the code at the position of the return and appends return instruction to end
      Instruction ireturn = rih.swapInstruction(new NOP());
      il.append(ireturn);
      body.append(rih, il);
    }

    // Updates the method.
    method.setMaxLocals();
    method.setMaxStack();
    method.update();
  }
}
