package jshadow.utils;

import org.apache.bcel.generic.MethodGen;

public class Utils {

  /**
   * Gets a signature string of the method that includes the package and class name, as well as parameter
   * types and the return type.
   * 
   * @param className
   *          The class that the method belongs to.
   * @param methodName
   *          The method to get the signature of.
   * @param paramSignature
   *          The signature of method parameters.
   * @return A signature string of the method.
   */
  public static String getSignature(String className, String methodName, String paramSignature) {
    return String.format("%s.%s%s", className, methodName, paramSignature);
  }

  /**
   * Gets a signature string of the method that includes the package and class name, as well as parameter
   * types and the return type.
   * 
   * @param m
   *          The method to get the signature of.
   * @return A signature string of the method.
   */
  public static String getSignature(MethodGen m) {
    return getSignature(m.getClassName(), m.getName(), m.getSignature());
  }
}
