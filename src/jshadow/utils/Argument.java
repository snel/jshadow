package jshadow.utils;

/**
 * Defines a terminal (or command-line) argument.
 * 
 * @author S. E. A. Nel
 */
public abstract class Argument implements Comparable<Argument> {

  protected boolean required = false;
  protected String[] switches;
  protected String usageString;

  /**
   * Constructor.
   */
  public Argument() {
  }

  /**
   * Sets the terminal switches (options) that activate this Argument.
   * 
   * @param switches
   *          Array of Strings, with each element being a permitted switch. For example, {-h, --help}
   * @return this
   */
  public Argument setSwitches(String[] switches) {
    this.switches = switches;
    return this;
  }

  /**
   * Gets the terminal switches (options) that activate this Argument.
   * 
   * @return the terminal switches
   */
  public String[] getSwitches() {
    return switches;
  }

  /**
   * Sets whether or not this is a required argument.
   * 
   * @param required
   *          Whether or not this is a required argument.
   * @return this
   */
  public Argument setRequired(boolean required) {
    this.required = required;
    return this;
  }

  /**
   * Returns true iff this is a required argument.
   * 
   * @return true iff this is a required argument.
   */
  public boolean isRequired() {
    return required;
  }

  /**
   * Sets the usage string, displayed with the usage instructions on the terminal.
   * 
   * @return this
   */
  public Argument setUsageString(String usageString) {
    this.usageString = usageString;
    return this;
  }

  /**
   * Gets the usage string of this argument.
   * 
   * @param val
   *          The value of this argument (may be null).
   * @return this
   */
  public String getUsageString() {
    StringBuilder s = new StringBuilder();
    s.append("  ");
    for (int i = 0; i < switches.length; i++) {
      s.append(switches[i]);
      if (i < switches.length - 1) {
        s.append(", ");
      }
    }
    s.append("    ");
    s.append(usageString);
    return s.toString();
  }

  /**
   * Sets the value of this argument.
   * 
   * @param val
   *          The value of this argument (may be null).
   * @return this
   */
  public abstract Argument set(String val);

  @Override
  public int compareTo(Argument o) {
    return switches[0].compareTo(o.switches[0]);
  }

  @Override
  public String toString() {
    return switches[0];
  }

}
