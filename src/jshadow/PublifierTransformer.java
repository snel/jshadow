package jshadow;

import java.lang.reflect.Modifier;

import org.apache.bcel.Constants;
import org.apache.bcel.classfile.Attribute;
import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.InnerClass;
import org.apache.bcel.classfile.InnerClasses;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.FieldGen;
import org.apache.bcel.generic.MethodGen;

/**
 * Makes all methods public for a given class.
 * 
 * @author Heila v.d. Merwe, S.E.A. Nel
 */
public class PublifierTransformer implements Transformer<ClassGen> {
  protected boolean isInner = false;

  /**
   * Applies the transformation to the specified class.
   * 
   * @param c
   *          Class to apply the transformation to.
   */
  @Override
  public void transform(ClassGen cg) {
    // if (!cg.getClassName().startsWith("android.content.Context")) {
    // return;
    // }

    // if this class is an annotation or EMUM ignore processing
    if (cg.isAnnotation() || cg.isEnum()) {
      System.out.println("Ignoring Enum/Annotation: \"" + cg.getClassName());
      return;
    }

    // publify class access
    int am = cg.getAccessFlags();
    System.out.println("**************Class: \"" + cg.getClassName() + "\" " + Integer.toBinaryString(am)
        + " " + printModifiers(am) + "**************");
    int am_new = updateMethodAccessModifiers(am);
    if (am_new != am) {
      cg.setAccessFlags(am_new);
      System.out.println("Class Updated: \"" + cg.getClassName() + "\" " + Integer.toBinaryString(am_new)
          + " " + printModifiers(am_new));
    }
    isInner = false;
    for (Attribute a : cg.getAttributes()) {
      if (a instanceof InnerClasses) {
        InnerClasses incs = (InnerClasses) a;
        for (InnerClass in : incs.getInnerClasses()) {
          isInner = true;
          am = in.getInnerAccessFlags();
          System.out.println(
              "InnerClass: " + cg.getClassName() + " " + Integer.toBinaryString(am) + printModifiers(am));

          am_new = updateMethodAccessModifiers(am);
          if (am_new != am) {
            in.setInnerAccessFlags(am_new);
            System.out.println("InnerClass Updated: " + cg.getClassName() + " "
                + Integer.toBinaryString(am_new) + " " + printModifiers(am_new));
          }
        }

      }
    }

    publify(cg);
  }

  /**
   * Makes all methods in the specified class public.
   * 
   * @param cg
   *          The class to modify.
   */
  protected void publify(ClassGen cg) {
    boolean emptyConst = false;

    for (Method m : cg.getMethods()) {
      ConstantPoolGen cpg = cg.getConstantPool();
      MethodGen gen = new MethodGen(m, cg.getClassName(), cpg);
      int am = gen.getAccessFlags();

      int am_new = updateMethodAccessModifiers(am);
      if (am_new != am) {
        System.out.println("Method: \"" + gen.getMethod().getName() + "\" " + Integer.toBinaryString(am) + " "
            + printModifiers(am));
        gen.setAccessFlags(am_new);
        gen.update();
        cg.replaceMethod(m, gen.getMethod());
        System.out.println("Method Updated: \"" + gen.getMethod().getName() + "\" "
            + Integer.toBinaryString(am_new) + " " + printModifiers(am_new));
      }

      if (m.getName().contains("<init>")) {
        if ((!isInner && gen.getArgumentNames().length == 0)
            || (isInner && !cg.isStatic() && gen.getArgumentNames().length < 2)
            || (isInner && cg.isStatic() && gen.getArgumentNames().length == 0)) {
          System.out.println("Has empty constructor: " + gen.getMethod().getSignature() + " "
              + printModifiers(gen.getAccessFlags()));
          emptyConst = true;
        }
      }
    }

    if (emptyConst == false) {
      if (isInner && !cg.isStatic()) {
        System.out.println("Skipping adding empty constructor to inner class - no static");
      } else {
        cg.addEmptyConstructor(Modifier.PUBLIC);
        System.out.println("Adding empty constructor");
      }
    }

    for (Field f : cg.getFields()) {
      if (f.getName().contains("$")) {
        System.out.println("Ignoring Field: \"" + f.getName() + "\"");
      } else {

        ConstantPoolGen cpg = cg.getConstantPool();
        int am = f.getAccessFlags();

        System.out.println(
            "Field: \"" + f.getName() + "\" " + Integer.toBinaryString(am) + " " + printModifiers(am));
        int am_new = updateFieldAccessModifiers(am);
        if (am_new != am) {
          FieldGen gen = new FieldGen(am_new, f.getType(), f.getName(), cpg);
          gen.update();
          cg.replaceField(f, gen.getField());
          System.out.println("Updated: \"" + f.getName() + "\" " + Integer.toBinaryString(am_new) + " "
              + printModifiers(am_new));
        }
      }
    }

    cg.update();
  }

  /**
   * Returns a string describing the modifiers packed into <code>am</code>.
   * 
   * @param am
   *          Modifier attribute field.
   * @return A string describing the modifiers.
   */
  protected String printModifiers(int am) {
    String s = (((am & (Constants.ACC_STATIC)) == Constants.ACC_STATIC) ? "static " : "")
        + (((am & (Constants.ACC_FINAL)) == Constants.ACC_FINAL) ? "final " : "")
        + (((am & (Constants.ACC_PRIVATE)) == Constants.ACC_PRIVATE) ? "private " : "")
        + (((am & (Constants.ACC_PROTECTED)) == Constants.ACC_PROTECTED) ? "protected " : "")
        + (((am & (Constants.ACC_PUBLIC)) == Constants.ACC_PUBLIC) ? "public " : "");
    return s;
  }

  /**
   * Returns the modifiers with private/protected/final flags disabled but with the public flag enabled. For
   * use with methods.
   * 
   * @param am
   *          The modifier attribute field.
   * @return The updated field.
   */
  protected int updateMethodAccessModifiers(int am) {
    am |= Constants.ACC_PUBLIC; // make it public
    am &= ~Constants.ACC_PRIVATE; // remove private flag
    am &= ~Constants.ACC_PROTECTED; // remove protected flag
    am &= ~Constants.ACC_FINAL; // remove final flag
    return am;
  }

  /**
   * Returns the modifiers with private/protected flags disabled but with the public flag enabled. UFor use
   * with fields.
   * 
   * @param am
   *          The modifier attribute field.
   * @return The updated field.
   */
  protected int updateFieldAccessModifiers(int am) {
    am |= Constants.ACC_PUBLIC; // make it public
    am &= ~Constants.ACC_PRIVATE; // remove private flag
    am &= ~Constants.ACC_PROTECTED; // remove protected flag
    // am &= ~Constants.ACC_FINAL; // remove final flag
    return am;
  }
}