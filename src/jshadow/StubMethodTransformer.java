package jshadow;

import java.util.logging.Level;

import org.apache.bcel.Constants;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.InstructionFactory;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUSH;
import org.apache.bcel.generic.ReferenceType;

import jshadow.utils.Utils;

/**
 * Creates stubs of native methods.
 * 
 * @author S. E. A. Nel
 */
public class StubMethodTransformer extends InstrumentInOutTransformer {

  /**
   * Constructor.
   */
  public StubMethodTransformer() {
  }

  /**
   * Applies the transformation to all classes.
   * 
   * @param c
   *          Class to apply the transformation to.
   */
  @Override
  public void transform(ClassGen c) {
    try {
      for (Method method : c.getMethods()) {
        MethodGen m = new MethodGen(method, c.getClassName(), c.getConstantPool());
        if (m.isNative()) {
          try {
            stubMethod(m, c);
            // Overrides the existing method.
            c.removeMethod(method);
            c.addMethod(m.getMethod());
            c.update(); // ?
          } catch (Exception e) {
            JShadow.getInstance().log(Level.SEVERE,
                String.format("Error stubbing %s.", Utils.getSignature(m)));
            throw (e);
          }
          m.update();
        }
      }
      c.update();
    } catch (Throwable e) {
      e.printStackTrace();
      System.exit(0);
    }
  }

  /**
   * Creates a stub of the given method.
   * 
   * @param method
   *          The method to stub.
   * @param c
   *          The class that the method belongs to.
   * @return method The stubbed method.
   */
  protected MethodGen stubMethod(MethodGen method, ClassGen c) {
    JShadow.getInstance().log(Level.FINER, String.format("Stubbing %s.", Utils.getSignature(method)));
    InstructionFactory factory = new InstructionFactory(c);
    method.setModifiers(method.getAccessFlags() & ~Constants.ACC_NATIVE);
    InstructionList body = method.getInstructionList();
    if (body == null) {
      body = new InstructionList();
      method.setInstructionList(body);
    }

    // Pushes a return value of the appropriate type on the stack.
    if (method.getReturnType() instanceof ReferenceType) {
      body.append(InstructionFactory.createNull(method.getReturnType()));
    } else {

      switch (method.getReturnType().getType()) {
      case Constants.T_VOID:
        break;
      case Constants.T_BOOLEAN:
        body.append(new PUSH(c.getConstantPool(), false));
        break;
      case Constants.T_BYTE:
        body.append(new PUSH(c.getConstantPool(), (byte) 0));
        break;
      case Constants.T_CHAR:
        body.append(new PUSH(c.getConstantPool(), (char) '0'));
        break;
      case Constants.T_SHORT:
        body.append(new PUSH(c.getConstantPool(), (short) 0));
        break;
      case Constants.T_INT:
        body.append(new PUSH(c.getConstantPool(), (int) 0));
        break;
      case Constants.T_LONG:
        body.append(new PUSH(c.getConstantPool(), (long) 0));
        break;
      case Constants.T_FLOAT:
        body.append(new PUSH(c.getConstantPool(), (float) 0));
        break;
      case Constants.T_DOUBLE:
        body.append(new PUSH(c.getConstantPool(), (double) 0));
        break;
      default:
        throw new RuntimeException(String.format("Unsupported type: %s", method.getReturnType()));
      }
    }

    // Inserts return statement. The return value is assumed to be at the top of the stack.
    body.append(InstructionFactory.createReturn(method.getReturnType()));

    // Updates the method.
    method.setMaxLocals();
    method.setMaxStack();
    method.update();

    return method;
  }
}
