package jshadow;

public interface Transformer<T> {

  /**
   * Performs some transformation on <code>obj</code>.
   * 
   * @param obj
   *          Object to perform transformation on.
   * @throws Exception
   *           If the transformation fails for any reason.
   */
  void transform(T obj) throws Exception;

}
