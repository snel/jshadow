package jshadow;

import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Level;

import jshadow.utils.Utils;

import org.apache.bcel.Constants;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.InstructionFactory;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.TargetLostException;

/**
 * Creates 'shadows' or Java method wrappers for native methods.
 * 
 * @author S. E. A. Nel
 */
public class ShadowMethodTransformer extends InstrumentInOutTransformer {
  public static String suffix = "_shadow";
  protected HashMap<String, MethodGen> methods;
  protected HashSet<String> shadowMethods;
  protected HashSet<String> shadowedMethods;
  public static boolean instrument = true;

  /**
   * Constructor.
   */
  public ShadowMethodTransformer() {
    methods = new HashMap<String, MethodGen>();
    shadowMethods = new HashSet<String>();
    shadowedMethods = new HashSet<String>();
  }

  /**
   * Gets the name of the shadow method corresponding the the given method.
   * 
   * @param method
   *          The method to get the shadow of.
   * @return The name of the shadow method corresponding the the given method
   */
  public static String renamed(MethodGen method) {
    return String.format("%s%s", method.getName(), suffix);
  }

  /**
   * Gets a method previously touched by this transformer. These methods include native methods and shadow
   * methods.
   * 
   * @param signature
   *          The method signature as returned by getSignature().
   * @return A method previously touched by this transformer.
   */
  public MethodGen getMethod(String signature) {
    return methods.get(signature);
  }

  /**
   * Tests whether the method is a shadow method created earlier.
   * 
   * @param method
   *          Signature of the method to test, as returned by getSignature().
   * @return true if the method is a shadow method created earlier.
   */
  public boolean isShadow(String method) {
    return shadowMethods.contains(method);
  }

  /**
   * Tests whether the method was shadowed earlier.
   * 
   * @param method
   *          Signature of the method to test, as returned by getSignature().
   * @return true if the method was shadowed earlier.
   */
  public boolean isShadowed(String method) {
    return shadowedMethods.contains(method);
  }

  /**
   * Returns true iff the method should be shadowed.
   * 
   * @param m
   *          Method to consider.
   * @return true iff the method should be shadowed
   */
  protected boolean shouldShadow(MethodGen m) {
    // We should not shadow constructors "<init>" or static clauses "<clinit>".
    return !(m.getName().equals("main") || m.getName().equals("<init>") || m.getName().equals("<clinit>"));
  }

  /**
   * Applies the transformation to all classes.
   * 
   * @param c
   *          Class to apply the transformation to.
   */
  @Override
  public void transform(ClassGen c) {
    try {
      for (Method method : c.getMethods()) {
        MethodGen m = new MethodGen(method, c.getClassName(), c.getConstantPool());
        if (shouldShadow(m)) {
          try {
            MethodGen shadow = shadowMethod(m, c);
            c.addMethod(shadow.getMethod());
            c.update(); // ?

            // Updates tables of visited methods.
            methods.put(Utils.getSignature(m), m);
            methods.put(Utils.getSignature(shadow), shadow);

            shadowedMethods.add(Utils.getSignature(m));
            shadowMethods.add(Utils.getSignature(shadow));
          } catch (Exception e) {
            JShadow.getInstance().log(Level.SEVERE,
                String.format("Error shadowing %s.", Utils.getSignature(m)));
            throw (e);
          }
          m.update();
        }
      }
      c.update();
    } catch (Throwable e) {
      e.printStackTrace();
      System.exit(0);
    }
  }

  /**
   * Shadows the given method: renames it, creates a new one with the same name, and makes the new one call
   * the old one.
   * 
   * @param method
   *          The method to shadow.
   * @param c
   *          The class that the method belongs to.
 * @throws TargetLostException 
   */
  protected MethodGen shadowMethod(MethodGen method, ClassGen c)  {
    StatementFactory sfactory = new StatementFactory(c);
    // Creates the new, non-native method.
    // TODO should also clear other non-concrete flags
    MethodGen shadow = new MethodGen(method.getAccessFlags() & ~Constants.ACC_NATIVE, method.getReturnType(),
        method.getArgumentTypes(), method.getArgumentNames(), renamed(method), method.getClassName(),
        new InstructionList(), method.getConstantPool());
    InstructionList body = shadow.getInstructionList();

    JShadow.getInstance().log(Level.FINER, String.format("Inserting %s.", Utils.getSignature(shadow)));

    // Inserts log statement .
    // String origin = String.format("Deferring from %s to %s.", shadow.getName(), method.getName());
    // sfactory.appendLog(body, origin);

    // Inserts deferred call to the shadowed method.
    sfactory.appendInvoke(body, method);

    // Inserts return statement. The return value is assumed to be at the top of the stack.
    body.append(InstructionFactory.createReturn(method.getReturnType()));

    // Instruments the method param and returns values.
    if (instrument) {
      shadow.setMaxStack();
      instrumentMethod(shadow, c);
    }

    // Updates the method.
    shadow.setMaxLocals();
    shadow.setMaxStack();
    shadow.update();

    return shadow;
  }
}
