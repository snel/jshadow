package jshadow;

import org.apache.bcel.Constants;
import org.apache.bcel.generic.ASTORE;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.InstructionFactory;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUSH;
import org.apache.bcel.generic.Type;

/**
 * Provides helper methods for generating BCEL instructions. TODO: The arrangement of method.setMaxLocals()
 * and method updates could be improved.
 * 
 * @author S. E. A. Nel, Heila
 */
public class StatementFactory {
  protected InstructionFactory factory;

  /**
   * Constructor.
   * 
   * @param c
   *          Class that the method body belongs to.
   */
  public StatementFactory(ClassGen c) {
    factory = new InstructionFactory(c);
  }

  /**
   * Adds an expression for invoking the specified method. Currently only public methods are supported.
   * 
   * @param body
   *          The InstructionList to append to.
   * @param method
   *          The method to invoke.
   */
  public void appendInvoke(InstructionList body, MethodGen method) {
    short kind;
    if (method.isStatic()) {
      kind = Constants.INVOKESTATIC;
    } else {
      // Adds an object reference.
      body.append(InstructionFactory.createThis()); // or
      // InstructionConstants.THIS

      // We assume that the method is never a constructor or interface
      // method etc.
      if (method.isPrivate()) {
        kind = Constants.INVOKESPECIAL;
      } else {
        kind = Constants.INVOKEVIRTUAL;
      }
    }

    // Pushes parameters onto the stack.method.getArgumentTypes()[0].
    int n = method.getArgumentTypes().length;
    if (n > 0) {
      for (int i = 0; i < n; i++) {
        appendPushParam(body, i, method.getArgumentTypes()[i], method);
      }
    }

    body.append(factory.createInvoke(factory.getClassGen().getClassName(), method.getName(),
        method.getReturnType(), method.getArgumentTypes(), kind));
  }

  /**
   * Adds instructions that store the parameters given to the method in a local variable, which is an Object
   * array. This must be placed at the start of the method body.
   * 
   * @param body
   *          The InstructionList to append to.
   * @param method
   *          The method that received the parameters.
   * @return The local variable the parameters are stored in.
   */
  public int appendStoreParams(InstructionList body, MethodGen method) {
    method.setMaxLocals();
    // push the result into the local variable
    int x = method.getMaxLocals();
    int n = method.getArgumentTypes().length;

    appendNewArray(body, n);
    for (int i = 0; i < n; i++) {
      body.append(InstructionFactory.createDup(Type.OBJECT.getSize()));
      Type type = method.getArgumentTypes()[i];

      body.append(factory.createConstant(i));
      if (type == Type.VOID) {
        body.append(factory.createConstant("<void>"));
      } else {
        appendPushParam(body, i, type, method);
        appendBox(body, type); // Boxes the parameter.
      }
      body.append(InstructionFactory.createArrayStore(Type.OBJECT));
    }

    org.apache.bcel.generic.LocalVariableGen lg = method.addLocalVariable("array",
        Type.getType(Object[].class), null, null);
    int localIndex = lg.getIndex();

    body.append(new ASTORE(localIndex));
    method.update();
    return appendGetParamString(body, method, localIndex);
  }

  public int appendGetParamString(InstructionList body, MethodGen method, int parameterValues) {
    org.apache.bcel.generic.LocalVariableGen lg = method.addLocalVariable("paramString", Type.STRING, null,
        null);
    method.setMaxLocals();
    int localIndex = lg.getIndex();

    // call flowlogger for string of params
    body.append(factory.createConstant(method.getSignature()));
    body.append(InstructionFactory.createLoad(Type.getType(Object[].class), parameterValues));

    // Invokes logger.
    body.append(factory.createInvoke("flowlogger.FlowLogger", "getParamString", Type.STRING, new Type[] {
        Type.STRING, Type.getType(Object[].class) }, Constants.INVOKESTATIC));

    body.append(new ASTORE(localIndex));
    return localIndex;
  }

  /**
   * Pushes the parameter onto the stack.
   * 
   * @param body
   *          InstructionList to append to.
   * @param i
   *          Parameter index (0-indexed).
   * @param type
   *          Type of the parameter.
   * @param method
   *          Method that the parameter belongs to.
   */
  protected void appendPushParam(InstructionList body, int i, Type type, MethodGen method) {
    // +1 for virtual methods because local variable 0 is 'this'!
    body.append(InstructionFactory.createLoad(type, method.isStatic() ? i : i + 1));
  }

  /**
   * Adds instructions that stores the return value of a method. This must be placed just after the native
   * method invocation, or before the return statement.
   * 
   * @param body
   *          InstructionList to append to.
   * @param method
   *          Method that the parameters belong to.
   * @return The local variable the return value is stored in.
   */
  public int appendStoreReturn(InstructionList body, MethodGen method) {
    method.setMaxLocals(); // Ensures the var count was updated.
    int x = method.getMaxLocals();

    Type type = method.getReturnType();
    if (type != Type.VOID) {
      body.append(InstructionFactory.createStore(type, x));
    }
    return x;
  }

  /**
   * Adds instructions that print the values of parameters and return value of a method.
   * 
   * @param body
   *          The InstructionList to append to.
   * @param parameterValues
   *          The local variable the parameter values were stored in (of type Object[]).
   * @param returnValue
   *          The local variable the parameter values were stored in (of type Object).
   * @param method
   *          The method that received the parameters.
   */
  public void appendLogMethod(InstructionList body, int parameterValues, int returnValue, MethodGen method) {
    body.append(factory.createConstant(factory.getClassGen().getClassName()));
    body.append(factory.createConstant(method.getName()));
    body.append(factory.createConstant(method.getReturnType().getSignature()));
    body.append(factory.createConstant(method.getSignature()));

    // Return value
    Type type = method.getReturnType();
    if (type == Type.VOID) {
      body.append(InstructionFactory.createNull(Type.OBJECT));
    } else {
      body.append(InstructionFactory.createLoad(type, returnValue));
      appendBox(body, type); // Boxes the parameter.
    }

    // Params
    body.append(InstructionFactory.createLoad(Type.STRING, parameterValues));

    // Invokes logger.
    body.append(factory.createInvoke("flowlogger.FlowLogger", "logMethod", Type.VOID, new Type[] {
        Type.STRING, Type.STRING, Type.STRING, Type.STRING, Type.OBJECT, Type.STRING },
        Constants.INVOKESTATIC));
  }

  /**
   * Adds instructions that print a string.
   * 
   * @param body
   *          InstructionList to append to.
   * @param il
   *          An InstructionList that pushes a String onto the stack.
   */
  public void appendLog(InstructionList body, InstructionList il) {
    // Prints to stdout.
    // body.append(factory.createFieldAccess("java.lang.System", "out", new
    // ObjectType("java.io.PrintStream"),
    // Constants.GETSTATIC));
    // body.append(il);
    // body.append(factory.createInvoke("java.io.PrintStream", "println",
    // Type.VOID, new Type[] { Type.STRING
    // },
    // Constants.INVOKEVIRTUAL));

    // Prints to FlowLogger.
    body.append(il);
    body.append(factory.createInvoke("flowlogger.FlowLogger", "log", Type.VOID, new Type[] { Type.STRING },
        Constants.INVOKESTATIC));
  }

  /**
   * Adds instructions that print the given string.
   * 
   * @param body
   *          InstructionList to append to.
   * @param str
   *          String to print.
   */
  public void appendLog(InstructionList body, String str) {
    InstructionList il = new InstructionList();
    il.append(new PUSH(factory.getConstantPool(), str));
    appendLog(body, il);
  }

  /**
   * Adds instructions that loads the variable at the given index and prints.
   * 
   * @param body
   *          InstructionList to append to.
   * @param index
   *          The index of the variable to print.
   * @param type
   *          The type of the variable.
   */
  public void appendLog(InstructionList body, int index, Type type) {
    InstructionList il = new InstructionList();
    il.append(new PUSH(factory.getConstantPool(), type.getSignature()));
    appendLog(body, il);
  }

  /**
   * Adds instructions to format a string.
   * 
   * @param body
   *          InstructionList to append to.
   * @param format
   *          The String format.
   * @param args
   *          Formatting arguments.
   */
  public void appendFormat(InstructionList body, String format, InstructionList... args) {
    body.append(new PUSH(factory.getConstantPool(), format));
    appendNewArray(body, args.length);
    for (int i = 0; i < args.length; i++) {
      appendAddArray(body, args[i], i);
    }
    body.append(factory.createInvoke("java.lang.String", "format", Type.STRING, new Type[] { Type.STRING,
        Type.getType(Object[].class) }, Constants.INVOKESTATIC));
  }

  /**
   * Adds instructions to box a variable. That is, it wraps an int in an Integer object, a byte in a Byte
   * object and so on.
   * 
   * @param body
   *          InstructionList to append to.
   * @param type
   *          The variable's current type.
   */
  public void appendBox(InstructionList body, Type type) {
    if (type == Type.INT) {
      body.append(factory.createInvoke("java.lang.Integer", "valueOf", Type.getType(Integer.class),
          new Type[] { Type.INT }, Constants.INVOKESTATIC));
    } else if (type == Type.SHORT) {
      body.append(factory.createInvoke("java.lang.Short", "valueOf", Type.getType(Short.class),
          new Type[] { Type.SHORT }, Constants.INVOKESTATIC));
    } else if (type == Type.BYTE) {
      body.append(factory.createInvoke("java.lang.Byte", "valueOf", Type.getType(Byte.class),
          new Type[] { Type.BYTE }, Constants.INVOKESTATIC));
    } else if (type == Type.LONG) {
      body.append(factory.createInvoke("java.lang.Long", "valueOf", Type.getType(Long.class),
          new Type[] { Type.LONG }, Constants.INVOKESTATIC));
    } else if (type == Type.FLOAT) {
      body.append(factory.createInvoke("java.lang.Float", "valueOf", Type.getType(Float.class),
          new Type[] { Type.FLOAT }, Constants.INVOKESTATIC));
    } else if (type == Type.DOUBLE) {
      body.append(factory.createInvoke("java.lang.Double", "valueOf", Type.getType(Double.class),
          new Type[] { Type.DOUBLE }, Constants.INVOKESTATIC));
    } else if (type == Type.CHAR) {
      body.append(factory.createInvoke("java.lang.Character", "valueOf", Type.getType(Character.class),
          new Type[] { Type.CHAR }, Constants.INVOKESTATIC));
    } else if (type == Type.BOOLEAN) {
      body.append(factory.createInvoke("java.lang.Boolean", "valueOf", Type.getType(Boolean.class),
          new Type[] { Type.BOOLEAN }, Constants.INVOKESTATIC));
    }
  }

  /**
   * Adds instructions to create a new object array. Leaves the array on the stack.
   * 
   * @param body
   *          InstructionList to append to.
   * @param args
   */
  public void appendNewArray(InstructionList body, int size) {
    body.append(factory.createConstant(size));
    body.append(factory.createNewArray(Type.OBJECT, (short) 1));
  }

  /**
   * Adds instructions to store the variable in an object array. Leaves the array on the stack.
   * 
   * @param body
   *          InstructionList to append the store instructions to.
   * @param il
   *          InstructionList containing instructions to push the variable onto the stack.
   * @param index
   *          Index into the array to store the variable.
   */
  public void appendAddArray(InstructionList body, InstructionList il, int index) {
    body.append(InstructionFactory.createDup(Type.OBJECT.getSize()));
    body.append(factory.createConstant(index));
    body.append(il);
    body.append(InstructionFactory.createArrayStore(Type.OBJECT));
  }
}
